const fetch = require("node-fetch");
let host = "https://erp.test.us-east-1.api.abas.ninja"; //https://erp.prod.eu-central-1.api.eu.abas.cloud;
let mandant;
let tenant;
let token;
let url;
const request = require("request");
const rp = require('request-promise');
let config;
let authMail;
let apiAccessToken
let aud;
/*
* Script, which generates the JWT token for authentication
*/




let checkLogInStatus = async (jovo) => {
  var text, data, token
  apiAccessToken = jovo.$request.getAccessToken()
  //AccessToken: Wird nach der Kontoverknüpfung zur Verfügung gestellt, Zugriff auf email und profil
  if (!apiAccessToken) {
    jovo.$alexaSkill.showAccountLinkingCard();
    text = ('Bevor sie fortfahren, verlinken Sie bitte ihren Account über die Alexa App',
    'Bevor sie fortfahren, verlinken Sie bitte ihren Account über die Alexa App, Sie können dies über die Alexa App machen.');
    return text 

  } else {   
    token = apiAccessToken;
    let options = {
      method: 'GET',
      uri: 'https://alexa-login.eu.auth0.com/userinfo',
      headers: {
        authorization: 'Bearer ' + token,
      }
    };
  
    //Account Daten
    await rp(options).then((body) => {
      data = JSON.parse(body);
    });
    
    //evtl Luca-Kaiser@live.com weil ich mich damit auf auth0 angemeldet habe
    authMail = data.email
    
    // if(Object.entries(erpCredentials).length === 0 && erpCredentials.constructor === Object) {
    //   text= "Bitte tragen sie zuerst Daten zu ihrem ERP System auf folgender Seite ein: "
    // } else{
    //Bearer Token for ERP
    text = ('Guten Tag, was kann ich für Sie tun?', 'Was möchten Sie tun? Sagen Sie zum Beispiel ich will die Rufnummer von Kunde Baumarkt.')

    //}
        return text
  }
}

let getData = async() => {
  //request to Dynamo DB for ClientID, ClientCredentials, Tenant, Email
  const erpCredentials = await getCredentials(apiAccessToken)

    
    mandant = erpCredentials.Item.mandant
    tenant = erpCredentials.Item.tenant
    
    await getConfigurationERPSystem();
    erpToken(erpCredentials)

}

let getConfigurationERPSystem = async (jovo) => {
  return new Promise(((resolve, reject) => {
  url = 'https://'+tenant+'/configuration.json'
  aud = 'https://'+tenant+'/'
  console.log(url)
  var options = { 
    method: 'GET',
    url: url,
    headers: 
    { Accept: 'application/json' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  config =  JSON.parse(body);
  
  if (error) {
    console.log("Error: ", error)
  }
  resolve() 
});
}))
}


function getCredentials(apiAccessToken){
  
  return new Promise(((resolve, reject) => {

    var options = {
      method: 'GET',
      url: 'https://d3w0oz35v3hvuk.cloudfront.net/profile',
      headers:
      {
        'cache-control': 'no-cache',
        Authorization: apiAccessToken,
        Accept: 'application/json',
        email: authMail
      }
    };
    request(options, function (error, response, body) {
      if (error) {
        console.log("Error: ", error)
        reject("Bitte tragen sie zuerst Daten zu ihrem ERP System auf folgender Seite ein.")
      };      
     
      resolve(JSON.parse(body) ) 
    });
  }))
}

let erpToken = async(cObjekt) => {

  return new Promise(((resolve, reject) => {
  console.log(config.api.authBaseURL+'/oauth/token == https://abas-dev.auth0.com/oauth/token')
  console.log(aud + " == https://crystal-labs.abas.ninja/")
  var options = {
    method: 'POST',
    url: config.api.authBaseURL+'/oauth/token',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    form: {
      grant_type: 'client_credentials',
      client_id: cObjekt.Item.clientid,
      client_secret: cObjekt.Item.clientsecret,
      audience: aud
    }
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    var obj = JSON.parse(body);
    token = "Bearer " + obj.access_token;
    console.log(token)
    resolve(token)
  });
  }))
}

/*
*   Retrives service tasks from a person specified in url and gives them back. All attributes are specified in the url
*/
let getServiceTasks = async (token) => 
{
  
  let tasks;

  console.log(host + '/mw/r/' + mandant + '/infosys/data/ser/SERAUO?kbserauo=1|ktechniker=(161,117,0)|kvom=.|kbis=.|');
  await fetch(host + '/mw/r/' + mandant + '/infosys/data/ser/SERAUO?kbserauo=1|ktechniker=(161,117,0)|kvom=.|kbis=.|', {
    method: 'POST',
    headers: {
      'Authorization': token,
      'Accept': 'application/abas.objects+json',
      'Accept-language': 'de-DE',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {"actions" : [
        {
          "_type" : "SetFieldValue",
          "fieldName" : "bstart",
          "value" : ""
        }
      ]
    })
  }).then( (resp) => processResponse(resp))
    .then( (resp) => {
      console.log("getServiceTasks: DONE fetching items....");
      
      if(typeof resp.status === "undefined")
      {
        console.log("getServiceTasks: Amount of Tasks = " + resp.content.data.table.length);
        tasks = resp.content.data.table;
      }
  });

  return tasks;
}

//retrives the task specified by id or the last task from the person specified in url and gives the task object back
let getSelectedTaskInfo = async (token, taskid) => 
{
  let match;
  await fetch(host + '/mw/r/' + mandant + '/infosys/data/ser/SERAUO?kbserauo=1|ktechniker=(161,117,0)|kvom=.|kbis=.|', {
    method: 'POST',
    headers: {
      'Authorization': token,
      'Accept': 'application/abas.objects+json',
      'Accept-language': 'de-DE',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {"actions" : [
        {
          "_type" : "SetFieldValue",
          "fieldName" : "bstart",
          "value" : ""
        }
      ]
    })
  }).then( (resp) => processResponse(resp))
    .then( (resp) => {
      console.log("getSelectedTaskInfo: DONE fetching items....");
      if(typeof resp.status !== "undefined")
        return;
      //retrive latest task
      if(typeof taskid === "undefined")
      {
        match = resp.content.data.table[resp.content.data.table.length-1].fields;
        console.log("getSelectedTaskInfo: Last Task URL = " + match.tvorgang.links.via[0].href);
      }
      //retrive task with the given taskid if id is over 200000
      else if(taskid > 200000)
      {
        let tasks = resp.content.data.table;
        tasks.forEach(function(task){
        if(task.fields.tvorgang.text === taskid)
        {
          match = task.fields;
          console.log("getSelectedTaskInfo: MATCH with taskid found");
        }
      })
      }
      //if the given id is under or equal to the amount of tasks, work with it as the task number from the list
      else if(taskid <= resp.content.data.table.length)
      {
        match = resp.content.data.table[taskid-1].fields;
        console.log("getSelectedTaskInfo: Last Task URL = " + match.tvorgang.links.via[0].href);
      }
  })
  return match;
}

/*
* Updates the task with the specified parameters and gives back a boolean. Can be invoked in 2 ways
* 1. Way: Invoked with taskurl
* 2. Way: Invoked with taskid
*/
let updateTaskStatus = async (token, taskid, taskurl, taskstatus) => 
{
  console.log("updateTaskStatus(): taskID = " + taskid + ", taskurl = " + taskurl + ", taskstatus = " + taskstatus);
  let success = false;
  if(typeof taskurl !== "undefined")
  {
    console.log("updateTaskStatus: Entering over taskURL...");
    await fetch(host + taskurl, {
      method: 'POST',
      headers: {
        'Authorization': token,
        'Accept': 'application/abas.objects+json',
        'Accept-language': 'de-DE',
        'Content-Type': 'application/abas.objects.simple+json'
      },
      body: JSON.stringify(
        {
          "head": {
              "serstatus":  taskstatus
          }
        })
    }).then( (resp) => processNonJSONResponse(resp))
      .then( (resp) => {
        console.log("updateTaskStatus: DONE fetching items....");
        if(resp.status === 200)
              success = true;
    })
  }
  else if(typeof taskid !== "undefined")
  {
    console.log("updateTaskStatus: Entering over taskId...");
    let match;
    await fetch(host + '/mw/r/' + mandant + '/infosys/data/ser/SERAUO', {
    method: 'POST',
    headers: {
      'Authorization': token,
      'Accept': 'application/abas.objects+json',
      'Accept-language': 'de-DE',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {"actions" : [
        {
          "_type" : "SetFieldValue",
          "fieldName" : "bstart",
          "value" : ""
        }
      ]
      })
    }).then( (resp) => processResponse(resp))
      .then( async (resp) => {
        console.log("updateTaskStatus: DONE fetching items....");
        if(typeof resp.status !== "undefined")
          return;

        let tasks = resp.content.data.table;
        tasks.forEach(function(task){
          if(task.fields.tvorgang.text === taskid)
          {
            match = task;
            console.log("updateTaskStatus: MATCH with taskid found");
          }
            
        })
        console.log("updateTaskStatus: Task URL = " + match.fields.tvorgang.links.via[0].href);
        //fetch with new url extracted from requested task number
        await fetch(host + match.fields.tvorgang.links.via[0].href, {
          method: 'POST',
          headers: {
            'Authorization': token,
            'Accept': 'application/abas.objects+json',
            'Accept-language': 'de-DE',
            'Content-Type': 'application/abas.objects.simple+json'
          },
          body: JSON.stringify(
            {
              "head": {
                "serstatus": taskstatus
              }
          })
        }).then( (resp) => processNonJSONResponse(resp))
          .then( (resp) => {
            console.log("updateTaskStatus: DONE fetching items....");
            if(resp.status === 200)
              success = true;
        })
    })
  }
  console.log("updateTaskStatus: SUCCESS = " + success);
  return success;
}

//processes the response object and generate response status and text debug messages. Also converts the resp to json if request status > 400
function processResponse(resp){
  console.log("RESPONSE STATUS = " + resp.statusText + " " +  resp.status);
  if(resp.status <= 400)
    return resp.json();
  else
    return resp;
}

//processes the response object and generate response status and text debug messages.
function processNonJSONResponse(resp){
  console.log("RESPONSE STATUS = " + resp.statusText + " " +  resp.status);
  return resp;
}

module.exports.getServiceTasks = getServiceTasks;
module.exports.updateTaskStatus = updateTaskStatus;
module.exports.getSelectedTaskInfo = getSelectedTaskInfo;
module.exports.checkLogInStatus = checkLogInStatus;
module.exports.getConfigurationERPSystem = getConfigurationERPSystem;
module.exports.getData = getData;