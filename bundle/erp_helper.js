module.exports = {
    //generates data for the ListTemplate1 from ASK presets.
    generateTaskList(tasks, listTemplate)
    {
        let background_image_url = "https://image.ibb.co/hpiGup/abas_black_board.png";
        listTemplate
            .setTitle("Ihre Aufträge")
            .setToken("taskList")
            .setBackgroundImage({
                description: 'Background',
                url: background_image_url,
            })
            .setBackButton("HIDDEN");
        tasks.forEach( function(item)
        {   
            listTemplate.addItem(
                item.fields.tvorgang.text,
                null,
                "Auftrag " + item.fields.tvorgang.text,
                "Produkt = " + item.fields.tserprodpt.text
            )
        })  

        return listTemplate;
    },

    generateSpeech(lastTask)
    {
        console.log("generateSpeech: ENTER");
        let speech = "Auftragsnummer ist <say-as interpret-as=\"digits\">" + lastTask.tvorgang.text + 
        "</say-as>";
        let customer = lastTask.tkundept.text ? ", Kunde ist " + lastTask.tkundept.text : "";
        let date = lastTask.tterm.text ? ", Datum ist " + lastTask.tterm.text : "";
        let type = lastTask.tartname.text ? ", Art ist " + lastTask.tartname.text : "";
        let product = lastTask.tserprodpt.text ? ", Produkt ist " + lastTask.tserprodpt.text : "";
        speech = speech + customer + date + type + product;
        console.log("generateSpeech: EXIT");
        return speech;
    },

    generateText(lastTask)
    {
        console.log("generateText: ENTER");
        let text = "<font size=\"3\">Auftragsnummer = " + lastTask.tvorgang.text;
        let customer = lastTask.tkundept.text ? " <br/>Kunde = " + lastTask.tkundept.text : "";
        let type = lastTask.tartname.text ? " <br/>Art = " + lastTask.tartname.text : "";
        let date =  lastTask.tterm.text ? " <br/>Datum = " + lastTask.tterm.text : "";
        let product = lastTask.tserprodpt.text ? " <br/>Produkt ist = " + lastTask.tserprodpt.text + "</font>" : "</font>";
        text = text + customer + date + type + product;
        console.log("generateText: EXIT");
        return text;
    }
};