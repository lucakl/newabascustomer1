// ------------------------------------------------------------------
// JOVO PROJECT CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
    alexaSkill: {
        nlu: 'alexa',
        manifest: {
         apis: {
             custom: {
                 interfaces: [
                     {
                         type: 'RENDER_TEMPLATE'
                     }
                 ]
             }
         }
       }
     },
    googleAction: {
       nlu: 'dialogflow',
    },
    endpoint: '${JOVO_WEBHOOK_URL}',
    
};
 