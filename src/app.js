'use strict';

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

let background_image_url = "https://image.ibb.co/hpiGup/abas_black_board.png";
const { App } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { JovoDebugger } = require('jovo-plugin-debugger');
const erp_helper = require('./erp_helper.js');
const dataprovider = require('./erp_dataprovider.js');
const app = new App();

app.use(
    new Alexa(),    
    new JovoDebugger(),
    
);
// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------
app.setHandler({

    //funktioniert
    async LAUNCH() {;
        this.$session.$data.redirected = 0
        if (!this.$request.getAccessToken()) {

            this.$alexaSkill.showAccountLinkingCard();
            this.ask(this.t('notLoggedIn.speech'), this.t('notLoggedIn.reprompt'))
            
        } else {
        await dataprovider.checkLogInStatus(this).then((resp) => {
            //if(resp.includes("ERP")){
            //    this.$alexaSkill.showSimpleCard('Hint: ', 'If you havent entered your ERP Credentials yes, please do it here: https://d3w0oz35v3hvuk.cloudfront.net/');
            //}
            //else {
                dataprovider.getData();
            //}  
            this.ask(this.t('welcome.speech'), this.t('welcome.reprompt'))
            })
        }
    },

    //funktioniert
    HelpIntent(){
        
        this.ask(this.t('help.speech'), this.t('help.reprompt'));
        //this.ask("Dieser Skill ist nur für ERP Kunden von abas geeignet. Mit der Hilfe dieses Skills, können Sie mich beispielsweise auffordern ihnen ihre aktuellen Aufgaben anzuzeigen, eine spezifische Aufgabe vorzulesen oder ihnen ihre Notizen anzuzeigen oder vorlesen zu lassen.")
    },

    //funktioniert
    async StartIntent() {

        if (this.$session.$data.redirected === 0) {
            this.ask(this.t('redirectOne.speech'), this.t('redirectOne.reprompt'));
        }
        else if (this.$session.$data.redirected === 1) {
            this.$session.$data.redirected = 0;
            this.ask(this.t('redirectTwo.speech'), this.t('redirectTwo.reprompt'));
        }
        else if (this.$session.$data.redirected === 2) {
            this.$session.$data.redirected = 0;
            this.ask(this.t('redirectThree.speech'), this.t('redirectThree.reprompt'));
        }
        else {
            this.ask('strange')
        }
    },

    //funktioniert nicht
    async TaskIntent() {
        console.log("-----------------TASKINTENT------------------")
        let that = this;
        let tasks;
        console.log("TaskIntent: ENTER");
        console.log("AUTH TOKEN = " + this.getSessionAttribute("authtoken"));
        await dataprovider.getServiceTasks(this.getSessionAttribute("authtoken")).then( (resp) => {
            console.log("RESP: " +resp)
            tasks = resp;
            if (typeof tasks === "undefined")
                that.ask(this.t('task.notFound'));
            else
            {
                let listTemplate1 = this.alexaSkill().templateBuilder('ListTemplate1');
                listTemplate1 = erp_helper.generateTaskList(tasks, listTemplate1);
                that.alexaSkill().showDisplayTemplate(listTemplate1);
                that.ask(this.t('task.speech', {taskNumber: tasks.length}));
            }
        });
    },

    //If user selects the task over voice or with touchscreen, the selection will be handlere in this intent
    async SelectedTask(){
        let lastTask;
        let that = this;
        taskid = this.$inputs.taskid.value;
        console.log("SelectedTask INTENT: TASKID.value = " + taskid + ", taskid = " + taskid);
        dataprovider.getSelectedTaskInfo(this.getSessionAttribute("authtoken"), taskid).then( (resp) => {
            if (typeof resp === "undefined")
                that.ask(this.t('selected.notFound'));
            else
            {
                lastTask = resp;
                let bodyTemplate2 = that.alexaSkill().templateBuilder('BodyTemplate2');
                bodyTemplate2
                .setToken('bodytoken')
                .setTextContent(erp_helper.generateText(lastTask))
                .setBackButton("HIDDEN")
                .setBackgroundImage({
                    description: 'Background',
                    url: background_image_url,
                });
                that.alexaSkill().showDisplayTemplate(bodyTemplate2);
                that.setSessionAttribute("taskURL", lastTask.tvorgang.links.via[0].href);
                that.setSessionAttribute("taskid", lastTask.tvorgang.text);

                that.ask(erp_helper.generateSpeech(lastTask));
            }
        });
    },

    //Intent which asks to confirm the task update process and redirects to confirmation state.
    async UpdateTask(){
        if(typeof this.$inputs.taskid.value !== "undefined" && typeof this.$inputs.taskstatus.value !== "undefined")
        {
            generateBackground(this, "Auftrag " +  taskid.value + " auf " + taskstatus.value + " setzen?");
            this.setSessionAttribute("taskid", this.$inputs.taskid.value);
            this.setSessionAttribute("taskstatus", this.$inputs.taskstatus.value);
            this.followUpState('ConfirmationState').ask(this.t('update.speech', {taskNumber: this.$inputs.taskid.value, taskStatus: taskstatus.value}))
            
        }
        else if(typeof this.$inputs.taskid.value === "undefined" && typeof this.getSessionAttribute("taskid") !== "undefined" && typeof this.$inputs.taskstatus.value !== "undefined")
        {
            generateBackground(this, "Auftrag " +  this.getSessionAttribute("taskid") + " auf " + this.$inputs.taskstatus.value + " setzen?");
            this.setSessionAttribute("taskstatus", this.$inputs.taskstatus.value);
            //this.followUpState('ConfirmationState').ask("Wollen Sie den Serviceauftrag mit der Nummer <say-as interpret-as=\"digits\">" + this.getSessionAttribute("taskid") + "</say-as> auf " + taskstatus.value + " setzen?");
            this.followUpState('ConfirmationState').ask(this.t('update.speechT', {taskNumberT: this.getSessionAttribute("taskid"), taskStatusT: taskstatus.value} ));
        }
        else
        {
            console.log("UpdateTask INTENT: TaskID = " + this.$inputs.taskid + ", taskStatus = " + this.$inputs.taskstatus);
            this.ask(this.t('update.notFound'));
        }    
    },

    //repromt user for confirmation of task update
    ConfirmationState: 
    {
        //if user say yes, the request will be proceeded and an attempt to update the value of the task will be started
        async YesIntent () {
            generateBackground(this);
            dataprovider.updateTaskStatus(this.getSessionAttribute("authtoken"), this.getSessionAttribute("taskid"), this.getSessionAttribute("taskURL"), this.getSessionAttribute("taskstatus"), ).then( (resp) => {
                if(resp === true){
                    this.ask(this.t('confirmation.speech', {taskNumber: this.getSessionAttribute("taskid"), taskStatus: this.getSessionAttribute("taskstatus")}))
                }
                    else{
                    this.ask(this.t('confirmation.canceled'));
                    }
            });
        },

        //if user denies the request, he will be redirected to the StartIntent
        async NoIntent() {
            this.setSessionAttribute("redirected", 2);
            this.toIntent('StartIntent');
        },
    },

    END() {
        this.tell(this.t("end.speech"));
    },

    async Unhandled() {
        this.setSessionAttribute("redirected", 1);
        this.toIntent('StartIntent');
    },

    async UnknownIntent() {
        this.setSessionAttribute("redirected", 1);
        this.toIntent('StartIntent');
    }
});

function generateBackground(that, text){
    let bodyTemplate2 = that.alexaSkill().templateBuilder('BodyTemplate2');
    if(typeof text !== "undefined")
    {
        bodyTemplate2
        .setToken('token')
        .setBackButton("HIDDEN")
        .setTextContent(text)
        .setBackgroundImage({
            description: 'Background',
            url: background_image_url,
        });
        that.alexaSkill().showDisplayTemplate(bodyTemplate2);
    }
    else{
        bodyTemplate2
        .setToken('token')
        .setBackButton("HIDDEN")
        .setBackgroundImage({
            description: 'Background',
            url: background_image_url,
        });
        that.alexaSkill().showDisplayTemplate(bodyTemplate2);
    }
}
module.exports.app = app;
