// ------------------------------------------------------------------
// APP CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
    logging: true,
 
    intentMap: {
       'AMAZON.StopIntent': 'END',
       'AMAZON.YesIntent' : 'YesIntent',
        'AMAZON.NoIntent'  : 'NoIntent',
        'AMAZON.HelpIntent': 'HelpIntent',
    },
    i18n: {
        resources: {
            'de-DE': require('../i18n/de-DE'),
            'en-US': require('../i18n/en-US')
        }
    }, 
 };
 